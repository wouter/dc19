# Meeting report

## Details

* Date: 2018-08-11
* Time: 10h00min - 13h00min
* Place: Jupter
* Presents:
  1. Angelo
  1. Cleber
  1. Daniel
  1. Leonardo
  1. Paulo
  1. Samuel

## Topics

### How it was DebConf18

We talked about our (Paulo, Daniel and Samuel) experience at DebConf18 and all
things we could learn.
