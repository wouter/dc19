# DebConf19 Repository

## Venue

* Federal University of Technology - Paraná (
Universidade Tecnológica Federal do Paraná - UTFPR)
* Curitiba
* Brazil

## Dates

* DebCamp: Sunday, July 14 –> Friday, July 19, 2019 
* OpenDay: Saturday, July 20, 2019 
* DebConf: Sunday, July 21 –> Sunday, July 28, 2019 
