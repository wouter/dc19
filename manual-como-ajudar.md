---
name: Como ajudar a organizar a DebConf19
---
# Como ajudar na organização da DebConf19

Este é um manual sobre como você pode ajudar na organização da
[DebConf19](https://debconf19.debconf.org) - 20a edição da Conferência Mundial
de Desenvolvedores(as) do Projeto Debian, que acontecerá em Curitiba no Campus
central da UTFPR - Universidade Tecnológia Federal do Paraná.

A versão resumida é:

* Se inscreva na lista [debian-br-eventos](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos)
* Entre no canal [#debconf19-br](irc://irc.debian.org/debconf19-br) no OFTC
* Acesse o repositório <https://salsa.debian.org/debconf-team/public/data/dc19/boards>

Veja a [divisão de papeis](https://wiki.debian.org/DebConf/19/TeamRoles) onde
precisamos de ajuda e você pode coloborar. Edite a wiki e insera o seu nome em
quantos times você quiser ajudar. Para isso,
[crie o seu usuário](https://wiki.debian.org/DebConf/19/TeamRoles?action=newaccount)

## Datas

O que estamos chamando de "DebConf", na verdade são três eventos que acontecem
em sequência durante 2 semanas. Os eventos e suas respectivas datas são:

* DebCamp: 14 a 19 de julho de 2019 (domingo a sexta-feira)
* Open Day: 20 de julho de 2019 (sábado)
* DebConf: 21 a 28 de julho de 2019 (domingo a domingo)

### O que é DebCamp

Durante a DebCamp os colaboradores do Projeto Debian se concentram em tarefas ou
problemas de forma ininterrupta. Esses dias são exclusivos para pessoas que
querem trabalhar, portanto não existe uma grade de programação com palestras,
debates e oficinas.

Alguns [times do Debian](https://wiki.debian.org/Teams) organizam *sprints* como
[esses](https://wiki.debconf.org/wiki/DebConf18/DebCamp#Sprints) que aconteceram
na DebConf18.

A organização da DebConf aproveita esses dias para preparar toda a infraestrutura
que será usada durante a Conferência, como por exemplo a rede wifi, os
equipamentos para transmissão e gravação das atividades, alojamento, etc.

### O que é Open Day

Durante o Open Day não pretendemos apenas mostrar o Debian para a comunidade
local, mas também queremos aprender com a comunidade local. Portanto, o Open Day
terá atividades com temas variados de interesse da comunidade FLOSS.

Haverá atividades como palestras, debates e oficinas sobre o Debian, e sobre
Software e Código Aberto em geral. A ideia do Open Day é ser parecido com outros
eventos que estamos acostumados a organizar em Curitiba como o
[FLISOL](https://flisol.info/FLISOL2018/Brasil) e o
[SFD - Software Freedom Day](https://www.softwarefreedomday.org).

## Onde se comunicar para ajudar

Antes de qualquer coisas é importante esclarecer que não fazemos nossas
discussões em grupos no Facebook, WhatsApp ou Telegram.

Então se você está disposto(a) a ajudar na organização da DebConf19, você terá
que usar ferramentas de comunicação que talvez você não esteja muito
acostumado(a), ou mesmo que talvez nunca tenha usado antes. Esperamos que você
se sinta desafiado(a) a usar essas ferramentas livres e que isso seja uma grande
oportunidade para você abrir novos horizontes. Desde já te convidamos para
conhecer esse "jeito Debian de fazer as coisas" :-)

Nossos trabalhos e comunicações são realizadas por meio de:

1. Listas de discussão por e-mail
1. Canais de IRC
1. Respositórios Git

A seguir explicaremos como usar cada uma dessas ferramentas.

## 1 - Lista de discussão por e-mail

Existem três listas de discussão que você deverá se inscrever. Se você não se
sente apto(a) a ler e escrever em inglês, não tem problema. Você pode se
inscrever apenas na primeira lista.

1) <debian-br-eventos@alioth-lists.debian.net>

Lista usada por nós do time local para discutir a organização da DebConf19.

As mensagens são em português e o histórico de mensagens está disponível em
<https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos>.
Você também pode usar essa página para se inscrever na lista.

2) <debconf-team@lists.debian.org>

Lista principal para as discussões da organização da DebConf onde participam os
membros do [Comitê global](https://wiki.debconf.org/wiki/DebConf_Committee),
pessoas que já organizaram uma DebConf no passado, e nós time local da DebConf19.

As mensagens são em inglês e o histórico de mensagens está disponível em
<https://lists.debian.org/debconf-team>. Você também pode usar essa página
para se inscrever na lista.

3) <debconf-discuss@lists.debian.org>

Lista usada pelo público em geral e organizadores da DebConf para enviar
informações, dúvidas, sugestões e reclamações. Qualquer pessoa que não está
envolvida na organização da DebConf19 pode participar.

As mensagens são em inglês e o histórico de mensagens está disponível em
<https://lists.debian.org/debconf-discuss>. Você também pode usar essa página
para se inscrever na lista.

## 2 - Canais de IRC

Para conversas em tempo real, você deverá se conectar em três canais do
servidor de IRC [OFTC.net](irc://irc.debian.org). Se você não se sente apto(a)
a ler e escrever em inglês, não tem problema. Você pode entrar apenas no
primeiro canal.

1) [#debconf19-br](irc://irc.debian.org/debconf19-br)

Canal usado por nós do time local para discutir em português a organização da
DebConf19.

Para entrar no canal use um cliente da sua preferência como HexChat ou
Konversation, ou pelo navegador
[webchat-#debconf19-br](https://webchat.oftc.net/?nick=&channels=#debconf19-br)

2) [#debconf-team](irc://irc.debian.org/debconf-team)

Canal principal usado para as discussões em inglês da organização da DebConf.

Para entrar no canal use um cliente da sua preferência como HexChat ou
Konversation, ou pelo navegador
[webchat-#debconf-team](https://webchat.oftc.net/?nick=&channels=#debconf-team)


3) [#debconf](irc://irc.debian.org/debconf)

Canal usado pelo público em geral e organizadores da DebConf para enviar
informações, dúvidas, sugestões e reclamações. Qualquer pessoa que não está
envolvida na organização da DebConf19 pode participar.

Para entrar no canal use um cliente da sua preferência como HexChat ou
Konversation, ou pelo navegador
[webchat-#debconf](https://webchat.oftc.net/?nick=&channels=#debconf)

### Como usar o IRC

O IRC é uma das soluções mais antigas para conversas em tempo real e é livre.
Mas diferente de soluções fechadas como WhatsApp e Telegram, ele tem o
inconveniente de não manter gravado o histórico das conversas quando você
encerra o seu cliente de IRC.

Se você está em um canal e resolve fechar o seu cliente IRC (por exemplo quando
desligar o computador), você perderá o histórico das conversas que aconteceram
durante a sua ausência.

Para resolver esse problena, você pode usar um serviço intermediário como o
[ZNC](https://znc.in) que manterá você conectado todo o tempo ao canal,
mesmo se você encerrar o seu cliente IRC.

Então você terá a seguinte configuração:

Cliente IRC (no seu computador) -> ZNC (no seu servidor)-> Servidor IRC (irc.debian.org)

Você deverá manter o ZNC ativo o tempo todo, por isso o ideal é instalar em um
servidor.

### Como usar Matrix + Riot

Se você não tem um servidor, ou não quer manter o ZNC ativo, a melhor solução
atualmente é usar o servidor público do projeto [Matrix](https://matrix.org).

O Matrix é uma alternativa livre de comunicação para o WhatsApp e o Telegram.
Além disso, você pode usá-lo como ponte entre o seu cliente e o servidor IRC.
Existem város [clientes](https://matrix.org/docs/projects/try-matrix-now.html),
disponíveis para você se conectar ao Matrix.

Um cliente recomendado é o [Riot](https://about.riot.im) que possue o app para
desktop e smartphone, e uma versão web.

No Riot você deverá [criar a sua conta](https://riot.im/app/#/register) .

Depis fazer o login, é preciso fazer a conexão com o Appservice do servidor
OFTC: na barra lateral esquerda, clique no ícone "Iniciar conversa". Quando
abrir caixa de diálogo, digite @oftc-irc:matrix.org e clique no botão
"Começar conversa".

Na barra lateral esquerda aparecerá na lista de pessoas a conexão
oftc-irc:matrix.org. Clique nela e então digite os canais do servidor OFTC você
quer entrar, usando os comandos a seguir:

/join #\_oftc\_#debconf19-br:matrix.org

/join #\_oftc\_#debconf:matrix.org

/join #\_oftc\_#debconf-team:matrix.org

Pronto, agora você está conectado aos canais da DebConf no servidor OFTC usando
a solução Matrix + Riot.

## 3 - Respositórios Git

O Debian mantem o seu próprio servidor git para hospedar os projetos. Ele é
chamado de [salsa](https://salsa.debian.org).

Lá é possível encontrar os três repositórios principais da DebConf19:

* Dados em geral (data/dc19)
* Patrocínios (sponsors/sponsors)
* Site (websites/dc19)

A seguir comentamos sobre cada um desses repositórios.

1) Dados em geral (data/dc19)

<https://salsa.debian.org/debconf-team/public/data/dc19/>

Este é o principal repositório porque contém os nossos arquivos de trabalho da
DebConf19 e o nosso kanboard com a lista de tarefas pendentes que podem ser
vistas em: <https://salsa.debian.org/debconf-team/public/data/dc19/boards>.

**Fique sempre de olho nesse kanboard**. Para começar a ajudar,
[crie o seu usuário](https://signup.salsa.debian.org/register/guest) e solicite
acesso ao repositório.

2) Patrocínios (sponsors/sponsors)

<https://salsa.debian.org/debconf-team/sponsors/sponsors>

Repositório para arquivos relacionados aos patrocínios. O acesso é restrito ao
time que irá trabalhar nessa área.

3) Site (websites/dc19)

<https://salsa.debian.org/debconf-team/public/websites/dc19>

Repositório para arquivos do site da DebConf19. Normalmente são poucas pessoas
do time que trabalham nessa área.

## Time local da DebConf19

**Venha fazer parte desse time você também :-)**

 * Adriana Cássia da Costa
 * Angelo Rosa
 * Antonio Terceiro
 * Arthur Del Esposte
 * Cleber Ianes
 * Daniel Lenharo de Souza
 * foz
 * Giovani Ferreira
 * Helen Koike
 * Lucas Kanashiro
 * Paulo Henrique de Lima Santana (phls)
 * Rodrigo Siqueira Jordão
 * Samuel Henrique
 * Valéssio Brito
