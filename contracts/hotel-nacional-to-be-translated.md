# CONTRACT

**CLAUSE ONE --**


*Paragraph One --* 43 rooms are reserved as follows:

* 11 tripe deluxe apartments: 03 single beds each.
* 22 family deluxe apartments: 02 double beds and 1 single bed each.
* 10 double deluxe apartments: 02 double beds each.

*Paragraph Two --* Addition of another 27 rooms reserved between July 20 and
29, to a total of 70 rooms, as follows:

* 02 quadruple deluxe apartments: 04 single beds each.
* 12 triple standard apartments: 03 single beds each.
* 12 double standard apartments: 01 double bed and 1 single bed each.
* 02 adapted deluxe apartments: 02 double beds each.

*Paragraph Three --* 04 rooms reserved between July 14th and 29th, as follows:

* Paraná room at 13th floor.
* Morretes and Antonia rooms at the 12th floor.
* Lapa room at the ground floor.

**CLAUSE TWO --**

Apartments:

1. Single apartments: R$ 153.00 per person per night.
1. Double, triple and quadruple apartments: R$ 85.00 per person per night.

*Paragraph Two --* The night fee include:

-   Breakfast.
-   Access to wifi internet.
-   Access to hotel facilities such as: sauna, gym, swimming pool.

*Paragraph Five --* The conference rooms will be provided free of charge as a
courtesy proportional to the amount of rooms below, to a maximum of 4 courtesy
fees per day.

1. Paraná room -- for each 25 occupied apartments per day.
1. Other rooms - for every 15 occupied apartments per day.

*Paragraph Six --* The CONTRACTING PARTY is authorized to sell their own beer
after 18:00 at the 13th floor; a cleaning fee of R$ 120.00 per night will be
charged to the conference master account

*Paragraph Seven --* It's also authorized the installation of an internet link
in the 12th and 13th floors, with the contracting party solely responsible for
the respective costs.

*Paragraph Eight --* The CONTRACTED PARTY will provide to the CONTRACTING PARTY
a promo code that will give a 5% discount to the fare being charged at the time
of the event, that can be used by additional guests that are not included in
the conference official list. (T.N.: i.e. self-paying attendees)

*Paragraph Nine --* The promo code  will be valid only for reservations made
through the hotel chain official website - www.nacionalinn.com.br - to any of
the hotels in the chain in Curitiba, and only during the period of the
conference: July 14th to 29th.

**CLAUSE FOUR --**

*Paragraph Second --* The CONTRACTING PARTY must forward the following
information to their guests:

* Hours: check-in after 14:00, checkout until 12:00.
* Breakfast is served at the restaurant between 06:00 and 10:00.

**CLAUSE FIVE --**

*Paragraph First --* The CONTRACTING PARTY confirms the amount of 43 rooms
between July 14th and 29th, 2019, plus 27 rooms between 20th and 29th, 2019, to
a total of 70 rooms; any cancellations or alterations will be made according to
the dispositions in this contract and upon request in writing.

*Paragraph Two --* To confirm the reservation it will be necessary to pay in
advance 70% of the total contracted amount, according to the schedule below,
with the remaining amount being paid until July 29th, 2019.

* Amount for July 14th to 29th: 43 rooms x 15 nights x 2 guests x R$ 85.00 =
  R$ 109,650.00.
* Amount for July 20th to 29th: 27 rooms x 09 nights x 2 guests x R$ 85.00 =
  R$ 41,310.00

Total:  R$ 150,960.00.

* 10% of the total amount on October 15th, 2018 -- R$ 15,096.00
* 30% of the total amount on March 1st, 2019 -- R$ 45,288.00
* 30% of the total amount on June 1st, 2019 -- R$ 45,288.00

70% of the payment in advance = R$ 105,672.00

*Paragraph Three --* For these calculations it is assumed two guests per room.
If there is more than two guests per room, the difference will be paid at the
last payment.

*Paragraph Four --* If the CONTRACTING PARTY doesn't show up at the reserved
dates, and does not cancel previously before the dates stipulated at the
"Cancellation policies" section, the CONTRACTED PARTY may charge for the full
amount budgeted for the event for the "no show", once the reservation is
confirmed.

*Paragraph Six --* The CONTRACTED PARTY retains the right to receive half of
the nights agreed upon here in case of "no show" in the hypothesis of the
CONTRACTING PARTY's schedule being altered by reasons beyond their control,
such as: adverse weather conditions, change in the schedule of relevant
attractions, acts of terrorism, burglaries or thefts, raise in fuel prices,
changes in the currency or government economic policies, etc.

**CLAUSE SIX --** Cancellation policies.

*Paragraph Third --* The parties agree with the deadline of January 31st for
the cancellation of all apartment and conference rooms without the incidency of
any penalties. In that case all amounts already paid will be returned via
transfer to the CONTRACTING PARTY account, minus any banking or credit card
fees charged in the operation.

*Paragraph Four --* Past the deadline, this contract can be cancelled upon
payment of a contractual penalty, in the following cases:

1. Cancellation before or on February 28th, 2019: charge of the full amount of
   the first payment.
1. Cancellation before or on June 20th, 2019: charge of the full amount of the
   first and second payments.
1. Cancellation before or on July 20th, 2019: charge of the full amount of the
   first three payments.

*Paragraph Five --* The confirmed reservation maybe be reduced in the following
way:

1. Before or on April 30th, 2019: no charge.
1. Before or on June 15th, 2019: there will be a charge of the first 05 nights
   of the apartments no longer reserved to a limit of 30% of the reserved
   apartments.
1. After June 16th, 2019: it will not be possible to reduce the reservation
   without payment of the full nightly rate of the apartments no longer
   reserved.

*Paragraph Six --* If the CONTRACTED PARTY decides for any reasons to cancel
the entire reservation, including but not limited to due the hotel closing
down, it will immediately refund all of the amount paid by the CONTRACTING
PARTY so far, return such amount via bank deposit to the CONTRACTING PARTY's
bank account.

*Paragraph Seven --* The parties may, in case of cancellation by the CONTRACTED
PARTY, reach an agreement and transfer the entire event and the corresponding
lodging to another chain hotel in Curitiba, in the same terms and amounts.

**CLAUSE SEVEN --** Modifications

*Paragraph Unique --* The parties may modify the clauses in this contract with
regards the number of apartments made available to the CONTRACTING PARTY
guests, amounts, periods, invoicing terms and payment dates, through numbered
contractual amendments, and at a minimum of 2 months before the event for the
case of substantial modifications.

**CLAUSE EIGHT --** Payment

*Paragraph One --* The payments agreed in the contract will be made via credit
card provided by the CONTRACTING PARTY.

*Paragraph Two --* For all intents and purposes, this contract totals R$
150,960.00
