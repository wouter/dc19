Title: DebConf Crowdfunding
Slug: debconf-crowdfuding
Date: 2019-05-31 12:30
Author: Daniel Pimentel
Artist: Catarse
Tags: debconf19, debconf, sponsors, crowdfunding
Lang: en
Status: published

# The project

The Debian Brasil Community created a crowdfunding campaign so that anyone can make a donation above R$ 10,00 (~ U$ 2.60) for the DebConf19 - Debian Project's Developers Conference realization.

This will be the 20th edition of the Conference and only the second time it will happen in Brazil!

We want to show how the Brazilian Debian community is strong, engaged and could be one of the official sponsors of DebConf19. If we hit the goals, the Debian Brasil logo will be placed in the sponsorship category equivalent to what is fundraised.

In the sponsorship plan of DebConf19 there are several quotas that your company can acquire one of them if you prefer. Our initial fundraising goal in this campaign is R$ 9.195,00 (~ U$ 2,384.00). If we disconsider Catharse's fee and the cost of production to make the t-shirt, we end up with R$ 7.000,00 (~ U$ 1.815,00), which is equivalent to a bronze quota (in Reais) in the sponsorship plan.

If we do not reach the initial goal, we will only be listed as supporters.

Remarks about the rewards:

* We will send the stickers for everyone in Brazil without additional cost.

* We can send the t-shirt anywhere, as long as the donor pays the shipping cost.

# Extended Goals

If we exceed our initial goal, the next step will be to try to reach R$ 21.265,00 (~ U$ 5,514.00) and thus acquire the silver quota (R$ 17.500,00 discounting the fee).

If we exceed the value of the silver quota, then the objective will be to reach the value of R$ 41.380,00 (~ U$ 10,731.00), which will put us in the gold quota (R$ 35,000.00 discounting the fee).

And if we exceed this amount, we will try the maximum amount of sponsorship, R$ 81.610,00 (~ U$ 21,163.00), which will put us in the platinum quota (R$ 70,000.00 discounting the fee).

We believe this is a dream that is possible to achieve, and we count on you to be part of it!

## Budget

Acquisition of the bronze quota of DebConf19: R$ 7.000,00. This amount will be used to pay expenses in the event.

## Rewards

Production: R$ 1.000,00 (~ U$ 259.00).

## Catarse's fee

13% of the total: R$ 1,195.00 (~ U$ 309.00).

# Values and what you get

* R$ 10,00 (~ U$ 2.59) or more: no rewards.

* R$ 20,00 (~ U$ 5.19) or more: you name listed in an "thank you" page on the website.

* R$ 30,00 (~ U$ 7.78) or more: you name listed in an "thank you" page on the website and 2 DebConf19 stickers.

* R$ 50,00 (~ U$ 12.97) or more: you name listed in an "thank you" page on the website, 2 DebConf19 stickers and 2 Debian stickers.

* R$ 100,00 (~ U$ 25.93) or more: you name listed in an "thank you" page on the website, 2 DebConf19 stickers, 2 Debian stickers, and 1 exclusive t-shirt like this: 
https://s3-sa-east-1.amazonaws.com/cdn.br.catarse/uploads/reward/uploaded_image/169646/thumb_reward_banner-catarse-camisa-2.jpg

If you donate R$ 20,00 or more, we will add your name on the Debian spiral printed in the t-shirt.

# Deadline

Crowdfunding campaign will finish 2019-06-15 23:59 (UTC -3). Run! :-)





