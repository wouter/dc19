# Dietas especiais solicitadas na DC18

http://paste.debian.net/hidden/be01b4de/

* nut allergy

* I don't eat seafood, fish or land meat is ok, vegetarian is ok

* I'm diabetic so would prefer to avoid carbs to the greatest extent possible. This means lots of vegetables, fish/meats, or dairy.

* I am alergic to pepper (as in the cooked vegetable, not the spice)

* For medical reasons, my doctor has ordered me to reduce intake of sugars, starches, and other non-fiber carbohydrates.  I don't want to impose a bunch of extra work on anyone, so I will do my best to get by no matter what is provided (even if this means skipping the DebConf provided meal and eating elsewhere).   But if meal options could include some sort of meat/fish/eggs (unbreaded with any sweet sauces served on the side) and a non-root, non-grain, vegetable, it would be great.  If you can't accomodate, I'll understand (In which case, please let me know so I can make other arrangements), but if you can, it would make me (and probably my doctor) really happy.  Thanks!

* No beef (Allergy)

* Hi, I am a muslim so it would be great if halal food is available, if its not available then it will still be not a problem as I am allowed to eat all types of sea food, fruits and vegetables (I cannot eat meat thats not halal unless its seafood).

* vegetarian, gluten-free, low carb/pre-diabetic. nothing will outright kill me; effects are cumulative; extended periods with inappropriate food makes it hard to think clearly.

* Paleo/keto/low-carb. (no rice, bread, pasta). I eat meat.

* VERY ALLERGIC to monosodium-glutamate (MSG) and gluten. Request a bit of meat with my vegetarian meals.

* Lactose Intolerate

* pescatarian (i.e. lacto-ovo vegetarian + fish + seafood)

* I am a vegetarian but I can take milk products. I take food free free from onion and garlic.

* I don't prefer eating coriander

* I do not eat pork, also any food that has alcohol in it.

